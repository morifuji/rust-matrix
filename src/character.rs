use rand;
use rand::Rng;
use rand::rngs::ThreadRng;
use termion::input::MouseTerminal;
use termion::{color};
pub type StdoutType = MouseTerminal<termion::raw::RawTerminal<std::io::Stdout>>;
const CHARS: &str = "abcdefghijklmnopqrstuvwxyzｦｧｨｩｪｫｬｭｮｯｰｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜﾝ1234567890-=*_+|:<>■";

#[derive(Debug, Clone)]
pub struct Character {
    char: char,
    is_bold: bool,
}

impl Character {
    pub fn new(rng: &mut ThreadRng) -> Character {
        let chars: Vec<char> = CHARS.chars().collect();
        let index = rng.gen_range(0, chars.len() - 1);

        Character {
            char: chars[index],
            is_bold: rng.gen_bool(0.5),
        }
    }

    pub fn display(
        & self,
        font_color: Box<dyn color::Color>,
    ) -> String {
        let style: Box<dyn std::fmt::Display> = if self.is_bold {
            Box::new(termion::style::Bold)
        } else {
            Box::new(termion::style::NoBold)
        };
        format!(
            "{}{}{}{}",
            color::Fg(&*font_color),
            (&*style),
            self.char,
            termion::style::Reset
        )
    }
}
