#[macro_use]
extern crate termion;

extern crate term_size;

use termion::input::MouseTerminal;
pub type StdoutType = MouseTerminal<termion::raw::RawTerminal<std::io::Stdout>>;
use rand::rngs::ThreadRng;
use std::io::{stdout, Write};
use termion::color;
use termion::raw::IntoRawMode;

pub mod character;
pub mod column;

fn main() {
    let mut stdout = MouseTerminal::from(stdout().into_raw_mode().unwrap());

    let dimension = term_size::dimensions();
    if dimension.is_none() {
        panic!("can't get dimention...");
    }

    let width = dimension.unwrap().0;
    let height = dimension.unwrap().1;

    let mut rng = rand::thread_rng();

    let mut column_list = (0..width)
        .map(|_| column::Column::new(height as u16, &mut rng))
        .collect::<Vec<_>>();

    loop {
        std::thread::sleep(std::time::Duration::from_millis(100));
        display(&mut stdout, &mut column_list, height);
        drop(&mut column_list);

        column_list = refresh(&mut column_list, height, &mut rng);
    }
}

fn refresh(
    column_list: &mut Vec<column::Column>,
    window_height: usize,
    rng: &mut ThreadRng,
) -> Vec<column::Column> {
    column_list
        .iter()
        .map(| column| {
            let head_position = column.head_position;
            let chars_size = column.chars.len();

            // そのまま
            if head_position < chars_size as u16 || window_height > (head_position - chars_size as u16) as usize {
                return column.clone();
            }

            // 新しく変更
            column::Column::new_with_top_head(window_height as u16, rng)
        })
        .collect()
}

fn display(stdout: &mut StdoutType, column_list: &mut Vec<column::Column>, window_height: usize) {
    write!(
        stdout,
        "{}{}{}",
        color::Fg(color::Reset),
        color::Bg(color::Reset),
        termion::clear::All
    )
    .expect("All clear error");
    column_list.iter().enumerate().for_each(|(x, column)| {
        let head_position = column.head_position;
        let chars_size = column.chars.len();
        if head_position >= chars_size as u16 && window_height < head_position as usize - chars_size {
            return;
        }
        column.chars.iter().enumerate().for_each(|(i, c)| {
            let color: Box<dyn color::Color> = if i == 0 {
                Box::new(color::White)
            } else {
                Box::new(color::Green)
            };
            if head_position < i as u16 {
                return;
            }
            write!(
                stdout,
                "{}{}",
                termion::cursor::Goto(1 + x as u16, 1 + head_position - i as u16),
                c.display(color)
            )
            .expect(&format!("can't display character {:?}", c));
        })
    });
    stdout.flush().unwrap();
}
fn drop(column_list: &mut Vec<column::Column>) {
    column_list.into_iter().for_each(|column| {
        column.drop();
    });
}

mod test {

    use super::*;
    use rand;
    use rand::rngs::ThreadRng;
    use rand::Rng;

    #[test]
    fn random() {
        let mut rng = rand::thread_rng();
        let mut rng2 = rand::thread_rng();
        dbg!(rng.gen_bool(0.0));
        dbg!(rng2.gen_bool(0.0));
    }
}
