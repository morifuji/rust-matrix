use super::character::Character;
use rand;
use rand::Rng;
use rand::rngs::ThreadRng;

#[derive(Debug, Clone)]
pub struct Column {
    pub head_position: u16,

    pub chars: Vec<Character>,
}

impl Column {
    pub fn new(height: u16, rng: &mut ThreadRng) -> Column {
        let length = rng.gen_range(1, height.div_euclid(2));
        let head_position = rng.gen_range(1, height);
        
        Column {
            head_position: head_position,
            chars: (0..length).map(|_| Character::new(&mut rand::thread_rng())).collect(),
        }
    }

    pub fn new_with_top_head(height: u16, rng: &mut ThreadRng) -> Column {
        let length = rng.gen_range(1, height.div_euclid(2));
        
        Column {
            head_position: 0,
            chars: (0..length).map(|_| Character::new(&mut rand::thread_rng())).collect(),
        }
    }

    pub fn drop(&mut self) {
        self.chars.pop();
        self.chars.insert(0, Character::new(&mut rand::thread_rng()));

        self.head_position += 1
    }
}

mod test {
    use super::*;
}
